import {defineStore} from "pinia";

export const useTestStore = defineStore('testStore', {
    state: () => ({
        counter: 0,
        results: [],
        parent_name: null,
        parent_age: null
    }),
    actions: {
        addCount() {
            this.counter++
        },
        removeCount() {
            this.counter--
        },
        sendForm(parent_name, parent_age, children) {
            this.results.push(`${parent_name}, ${parent_age}`, children);
        }
    },

})